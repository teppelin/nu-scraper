const jackrabbit = require("@pager/jackrabbit");
const methods = require("./methods");
const globalAgent = require("global-agent");

const { AMQP_URI } = process.env;
if (!AMQP_URI) {
  throw new Error("'AMQP_URI' env variable required.");
}

globalAgent.bootstrap();
const rbt = jackrabbit(process.env.AMQP_URI);

const onMessage = async (data, ack) => {
  ack();

  if (Buffer.isBuffer(data)) {
    data = JSON.parse(data);
  }

  console.log(data);

  // type method = "genres" | "tags" | "novel" | "novels" | "latestNovels"
  // { "method": "tags" }
  // { "method": "media", "params": { "id": 1337 } }
  // { "method": "medias", "params": { "offset": 9000, concurrency: 2 } }

  const exch = rbt.direct();
  exch.queue({ name: "inscribe", durable: true });
  const ctx = { params: data.params, exch };

  try {
    if (!(data.method in methods)) throw new Error("invalid method specified");
    const res = await methods[data.method](ctx);
    console.log(
      `✔️ done.`,
      Array.isArray(res) ? res.reduce((p, c) => p + (c | 0), 0) : res
    );
  } catch (err) {
    console.error(err);
  }
};

rbt
  .default()
  .queue({
    durable: false,
    name: "novelupdates_jobs",
  })
  .consume(onMessage);

console.log("✔️ Connected and listening for jobs.");

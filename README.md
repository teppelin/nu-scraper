# nu-scraper

[![Build Status](https://drone.miraris.moe/api/badges/teppelin/nu-scraper/status.svg)](https://drone.miraris.moe/teppelin/nu-scraper)

## Table of Contents

- [About](#about)
- [Getting Started](#getting-started)
- [Usage](#usage)

## About

[Novel Updates](https://www.novelupdates.com/) scraper in node.js

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See [deployment](#deployment) for notes on how to deploy the project on a live system.

### Prerequisites

Node.js or Docker

### Usage

clone the repo and build docker image, or run w/ node

```sh
export AMQP_URI=amqp://admin:secret@localhost//
node index.js
```

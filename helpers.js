const ISO6391 = require("iso-639-1");
const _ = require("lodash"); // yes, i got tilted

_.mixin({
  pascalCase: _.flow(_.camelCase, _.upperFirst),
});

const normalizeList = xs =>
  xs.flat().map(x => _.mapKeys(x, (_k, v) => _.pascalCase(v)));

const cover = c =>
  c !== "https://www.novelupdates.com/img/noimagefound.jpg" ? c : null;

const language = l => {
  switch (l) {
    case undefined:
    case null:
    case "":
      return "x-unk";
    case "Filipino":
      return "tl";
    case "Malaysian":
      return "ms";

    default:
      return ISO6391.getCode(l) || "x-unk";
  }
};

module.exports = { cover, language, normalizeList };

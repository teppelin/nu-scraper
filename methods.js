const nu = require("novelupdates.js");
const TurndownService = require("turndown");
const { cover, language, normalizeList } = require("./helpers");
const pLimit = require("p-limit");
const { debug } = require("debug");

const turndownService = new TurndownService();

/**
 *
 *
 * @param {*} msg
 * @param {*} r
 * @param {*} exch
 * @returns
 */
async function sendMessage(msg, r, exch) {
  try {
    debug("methods:sendMessage")("sending %s", msg.ID);
    exch.publish(msg, { appId: process.env.APP_ID || String(2), type: r });

    return true;
  } catch (err) {
    console.error(err, err.options);
  }
  return false;
}

/**
 *
 * @param {*} param0
 */
const genres = async ({ exch }) =>
  sendMessage(normalizeList(await nu.genresList()), "genres", exch);

/**
 *
 *
 * @param {*} { exch }
 * @returns
 */
async function tags({ exch }) {
  const res = [];

  for (let i = 1, max = 10; i <= max; i++) {
    const ts = await nu.tagsList({ page: i, sort: "abc", order: "asc" });
    max = ts.pageInfo.last;

    res.push(ts.data);
  }

  return sendMessage(normalizeList(res), "tags", exch);
}

/**
 *
 *
 * @param {*} { params: { id }, exch }
 * @returns
 */
async function novel({ params: { id }, exch }) {
  const novel = await nu.novel(id);
  const StatusInCOO = novel.statusCOO.includes(
    `<span class="seriesna">N/A</span>`
  )
    ? null
    : novel.statusCOO.map(x => x.trim()).join("\n");

  const MediaState = novel.completelyTranslated
    ? "completed"
    : /ongoing/gim.test(StatusInCOO)
    ? "ongoing"
    : "unaired/unknown";

  const yearStr = novel.year.toString();
  const msg = {
    ID: novel.id,
    Cover: cover(novel.cover),
    Titles: [
      { Title: novel.title, Type: "main" },
      ...novel.associatedNames.map(t => ({
        Title: t,
        Type: /^[A-Z]+$/.test(t) ? "short" : "synonym",
      })),
    ],
    Category: "novel",
    MediaState,
    MediaType: novel.type.type.toLowerCase(),
    Synopsis: turndownService.turndown(novel.rawSynopsis),
    RelatedMedia: novel.related.map(m => ({ Title: m.title, Type: m.type })),
    RecommendedMedia: novel.recommendations.map(m => ({
      Title: m.title,
      Votes: m.votes,
    })),
    Genres: novel.genres.map(x => x.name),
    Tags: novel.tags.map(t => t.name),
    Ratings: novel.ratings,
    Language: novel.languages.length
      ? language(novel.languages[0].name)
      : "x-unk",
    Writers: novel.authors.map(x => x.name),
    Illustrators: novel.artists.map(x => x.name),
    StartDate: yearStr.length === 4 ? yearStr : null,
    StatusInCOO,
    Licensed: novel.licensed,
    CompletelyTranslated: novel.completelyTranslated,
    MediaPublishers: novel.originPublisher.map(p => p.name),
  };

  return sendMessage(msg, "media", exch);
}

/**
 * This needs to be heavily optimized, right now it just iterates over all
 * novels, I implemented the latest-series parsing in the novelupdates.js
 * wrapper, so maybe use that.
 *
 * @param {*} { params, exch }
 * @returns
 */
async function novels({ params, exch }) {
  const opts = {
    first: undefined,
    offset: 0,
    concurrency: 3,
    ...params,
  };
  const firstPages = Math.ceil(opts.first / 25);
  let lastPage = 5;
  const limit = pLimit(opts.concurrency);
  const results = [];

  for (let i = 1; i <= (firstPages || lastPage); i++) {
    const res = await nu.novelsList({
      page: i,
      sort: "2",
      order: "1",
      status: "1",
    });
    lastPage = res.pageInfo.last;

    results.push(
      ...res.data.map(s => limit(() => novel({ params: { id: s.id }, exch })))
    );
  }

  return Promise.all(results);
}

/**
 * @param {*} { params, exch }
 * @returns
 */
async function latestNovels({ params, exch }) {
  const opts = {
    offset: 0,
    concurrency: 3,
    lookAhead: 3,
    ...params,
  };
  const limit = pLimit(opts.concurrency);
  const results = [];
  let depth = 0;

  pages: for (let p = 1; ; p++) {
    let low = false;
    const res = await nu.latestNovelsList(p);
    let [{ id: firstId }] = res.data;
    debug("methods:latestNovels:latestId")("%d", firstId);

    for (const { id } of res.data) {
      if (id === opts.offset) {
        debug("methods:latestNovels:found")("found match (%d), breaking", id);
        break pages;
      }
      if (id <= opts.offset) {
        low = true;
        continue;
      }
      results.push(limit(() => novel({ params: { id }, exch })));
    }
    if (depth === opts.maxDepth) {
      debug("methods:latestNovels:maxDepth")(
        "reached max allowed depth (%d), breaking",
        depth
      );
      break;
    }
    if (low) depth++;
  }

  return Promise.all(results);
}

module.exports = {
  genres,
  tags,
  novel,
  novels,
  latestNovels,
};
